import addEventListener = require("add-dom-event-listener");
import url = require("url");
import { createLocation } from "../lib";

const { parse } = url;

interface Options {
  html5Mode: boolean;
}

export const createOnLoad = (options: Options = { html5Mode: false }) => (
  _: Event
): boolean | null => {
  const handler = (url: url.UrlWithParsedQuery) => {
    switch (url.pathname) {
      case "/four":
        // redirect to home, rejecting with null will ignore the request
        return Promise.reject(parse("/", true));
      case "/error":
        // reject request by throwing an error
        throw new Error("Rejected request Error");
      default:
        return Promise.resolve(url);
    }
  };

  const location = createLocation(window, {
    html5Mode: options.html5Mode,
    handler
  });

  location.on("set", url => {
    console.log(`Events set ${url.pathname}`);
  });
  location.on("success", url => {
    console.log(`Events success ${url.pathname}`);
  });
  location.on("error", error => {
    console.log(`Events error ${error}`);
  });
  location.on("forward", () => {
    console.log(`Events forward`);
  });
  location.on("back", () => {
    console.log(`Events back`);
  });

  ["back", "forward"].forEach(id => {
    const element = window.document.getElementById(id);

    addEventListener(element as EventTarget, "click", (e: Event) => {
      e.preventDefault();

      if (id === "back") {
        location.back();
      } else {
        location.forward();
      }

      return false;
    });
  });

  ["one", "two", "three", "four", "error"].forEach(id => {
    const element = window.document.getElementById(id);

    if (element) {
      const pathname = element.dataset.pathname || "";

      addEventListener(element as EventTarget, "click", (e: Event) => {
        e.preventDefault();
        location.set(pathname);
        return false;
      });
    }
  });

  location.init();

  return null;
};
