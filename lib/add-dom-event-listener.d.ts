declare module "add-dom-event-listener" {
  function addEventListener(
    element: EventTarget,
    name: string,
    callback: (event: any) => boolean | undefined
  ): {
    remove(): void
  };
  export = addEventListener;
}
